# Table of Contents
[[_TOC_]]

# About
The message and service definitions for the 6D_flirt object pose tracking.
Compile this project as part of your catkin workspace to generate the messages for the 6D object pose tracker.

# Setup
After setting up the workspace, the dependencies can be installed via rosdep:
```bash
rosdep install --from-paths src --ignore-src -r -y
```
# Citing
If you use this library in your scientific publication, please consider citing:
```
@article { 3Dcamerabasedmarkerlessnavigationsystemforroboticosteotomies,
      author = "Tim Übelhör and Jonas Gesenhues and Nassim Ayoub and Ali Modabber and Dirk Abel",
      title = "3D camera-based markerless navigation system for robotic osteotomies",
      journal = "at - Automatisierungstechnik",
      year = "01 Oct. 2020",
      publisher = "De Gruyter",
      address = "Berlin, Boston",
      volume = "68",
      number = "10",
      doi = "https://doi.org/10.1515/auto-2020-0032",
      pages=      "863 - 879",
      url = "https://www.degruyter.com/view/journals/auto/68/10/article-p863.xml"
}
```

# Funding
Funded by the Excellence Initiative of the German federal and state governments.
