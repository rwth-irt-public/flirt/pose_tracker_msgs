#! /usr/bin/env python
""" Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
    All rights reserved. """

import rospy
from pose_tracker_msgs.srv import InitTracker, InitTrackerRequest

if __name__ == '__main__':
    rospy.init_node('test_init_tracker', anonymous=True)
    rospy.wait_for_service('init_tracker')
    init_tracker = rospy.ServiceProxy('init_tracker', InitTracker)
    request = InitTrackerRequest()
    request.transform_stamped.header.frame_id = 'camera_depth_optical_frame'
    request.transform_stamped.child_frame_id = 'whatever'
    request.transform_stamped.header.stamp = rospy.Time.now()
    # normalized quaternion
    request.transform_stamped.transform.rotation.w = 1
    request.transform_stamped.transform.translation.z = 0.5
    request.replacement_ratio = 0.5
    result = init_tracker(request)
    print result
