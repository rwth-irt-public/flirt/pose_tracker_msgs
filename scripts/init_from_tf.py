#!/usr/bin/env python
""" Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
    All rights reserved. """

import geometry_msgs.msg
import rospy
from pose_tracker_msgs.srv import InitTracker, InitTrackerRequest
import tf2_ros

if __name__ == '__main__':
    rospy.init_node('init_from_tf', anonymous=True)

    base_frame = rospy.get_param('~base_frame', 'world')
    init_frame = rospy.get_param('~init_frame', 'imarker')
    rate = rospy.get_param('~rate', 5)
    replacement_ratio = rospy.get_param('~replacement_ratio', 0.5)
    service_name = rospy.get_param('~service_name', 'init_tracker')

    tf_buffer = tf2_ros.Buffer()
    tf_listener = tf2_ros.TransformListener(tf_buffer)
    rospy.wait_for_service(service_name)
    init_tracker = rospy.ServiceProxy(service_name, InitTracker)

    sleep_rate = rospy.Rate(rate)
    while not rospy.is_shutdown():
        try:
            transform = tf_buffer.lookup_transform(
                base_frame, init_frame, rospy.Time(), rospy.Duration(rate))
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            sleep_rate.sleep()
            continue
        request = InitTrackerRequest()
        request.transform_stamped = transform
        request.replacement_ratio = replacement_ratio
        try:
            result = init_tracker(request)
        except rospy.service.ServiceException:
            continue
        try:
            sleep_rate.sleep()
        except rospy.exceptions.ROSTimeMovedBackwardsException:
            continue
